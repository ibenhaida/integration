<?php
namespace App\Tests;
 
use App\Entity\Demo;
use PHPUnit\Framework\TestCase;
 
class UnitTest extends TestCase
{
    public function testDemo()
    {
        $demo = new Demo();

        $demo->setName('name');

        $demo->setDescription('description'); 

 
        $this->assertTrue($demo->getName() === 'name');
    }
}
